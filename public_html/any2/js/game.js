/**
 * JavaScript Document
 */

/**
 * Block
 * @constructor
 */
function Blocks() {
    this.places = [
        [,,,,],
        [,,,,],
        [,,,,],
        [,,,,]
    ];
    var makeMove = false;
    this.move = function(dir) {
        var currRow = 0;
        var currCol = 0;
        var moveBoxes = function(piece, grid) {
            if (currPiece != null) {
                while (currPiece.canMove(dir, grid)) {
                    currPiece.moveOnce(dir, grid);
                    makeMove = true;
                }
                var neighbor = currPiece.getAdjacentBlocks(dir, grid);
                if (currPiece.isPossibleMerge(neighbor)) {
                    currPiece.collaps(neighbor, grid);
                    makeMove = true;
                }
            }
        };

        switch (dir) {
            case "down":
                currRow = this.places.length - 1;
                for (var col = currCol; col < this.places[0].length; col++) {
                    for (var row = currRow; row >= 0; row--) {
                        var currPiece = this.places[row][col];
                        moveBoxes(currPiece, this);
                    }
                }
                break;
            case "up":
                currRow = 0;
                for (var col = currCol; col < this.places[0].length; col++) {
                    for (var row = currRow; row < this.places.length; row++) {
                        var currPiece = this.places[row][col];
                        moveBoxes(currPiece, this);
                    }
                }
                break;
            case "left":
                currCol = 0;
                for (var row = currRow; row < this.places.length; row++) {
                    for (var col = currCol; col < this.places[0].length; col++) {
                        var currPiece = this.places[row][col];
                        moveBoxes(currPiece, this);
                    }
                }
                break;
            case "right":
                currCol = this.places[0].length - 1;
                for (var row = currRow; row < this.places.length; row++) {
                    for (var col = currCol; col >= 0; col--) {
                        var currPiece = this.places[row][col];
                        moveBoxes(currPiece, this);
                    }
                }
                break;
        }

        if (makeMove) {
            if (Math.random() < 0.5) {
                this.addBlock(new Boxes(2));
            } else {
                this.addBlock(new Boxes(4));
            }
        }
        makeMove = false;
    };

    /**
     * Add at random position
     * @param piece
     */
    this.addBlock = function(piece) {
        var emptyPositions = [];
        for (var i = 0; i < this.places.length; i++) {
            for (var j = 0; j < this.places[i].length; j++) {
                if (this.places[i][j] == null) {
                    emptyPositions.push([i, j]);
                }
            }
        }
        var randomPos = emptyPositions[Math.floor(Math.random() * emptyPositions.length)];
        this.places[randomPos[0]][randomPos[1]] = piece;
        piece.row = randomPos[0];
        piece.col = randomPos[1];
    };

    this.addPieceToPos = function(piece, row, col) {
        this.places[row][col] = piece;
        piece.row = row;
        piece.col = col;
    };

    this.canEndGame = function() {
        var g = this;
        var isGameEnd = function(piece) {
            if (piece === -1) {
                return true;
            }
            else if (piece == null) {
                return false;
            }
            else {
                var rightSideBlock = piece.getAdjacentBlocks("right", g);
                var bottomBlockItem = piece.getAdjacentBlocks("down", g);
                return !piece.isPossibleMerge(rightSideBlock) && !piece.isPossibleMerge(bottomBlockItem)
                    && isGameEnd(rightSideBlock) && isGameEnd(bottomBlockItem);
            }
        };
        return isGameEnd(this.places[0][0]);
    };

    this.toString = function() {
        var returned = "";
        for (var row = 0; row < this.places.length; row++) {
            for (var col = 0; col < this.places[row].length; col++) {
                var currPiece = this.places[row][col];
                if (currPiece == null) {
                    returned = returned + "0"
                } else {
                    returned = returned + currPiece.getValue();
                }
                returned = returned + " ";
            }
            returned = returned + "\n";
        }
        return returned;
    };
}

/**
 * Boxes class
 * @param val
 * @constructor
 */
function Boxes(val) {
    this.value = val;
    this.row = -1;
    this.col = -1;
    this.getValue = function() {
        return this.value;
    };

    this.addValue = function(o) {
        o.value += this.value;
    };

    this.collaps = function(other, matrix) {
        this.addValue(other);
        matrix.places[this.row][this.col] = null;
    };

    /**
     * Checks whether possible to merge or not
     * @param other
     * @returns {boolean}
     */
    this.isPossibleMerge = function(other) {
        return other != null && other != -1 && this.value === other.getValue();
    };

    /**
     * Checks whether possible to move or not
     * @param dir
     * @param grid
     * @returns {boolean}
     */
    this.canMove = function(dir, grid) {
        return this.getAdjacentBlocks(dir, grid) == null;
    };

    /**
     *
     * @param dir
     * @param grid
     */
    this.moveOnce = function(dir, grid) {
        switch (dir) {
            case "down":
                var currRow = this.row + 1;
                var currCol = this.col;
                break;
            case "up":
                var currRow = this.row - 1;
                var currCol = this.col;
                break;
            case "left":
                var currRow = this.row;
                var currCol = this.col - 1;
                break;
            case "right":
                var currRow = this.row;
                var currCol = this.col + 1;
                break;
        }

        grid.places[this.row][this.col] = null;

        grid.places[currRow][currCol] = this;
        this.row = currRow;
        this.col = currCol;
    };

    /**
     * Get the adjacent blocks
     * @param dir
     * @param grid
     * @returns {*}
     */
    this.getAdjacentBlocks = function(dir, grid) {
        var isPresentInBounds = function(row, col) {
            return row < grid.places.length && row >= 0 && col < grid.places[row].length && col >= 0;
        };

        switch (dir) {
            case "down":
                var currRow = this.row + 1;
                var currCol = this.col;
                break;
            case "up":
                var currRow = this.row - 1;
                var currCol = this.col;
                break;
            case "left":
                var currRow = this.row;
                var currCol = this.col - 1;
                break;
            case "right":
                var currRow = this.row;
                var currCol = this.col + 1;
                break;
        }

        if (!isPresentInBounds(currRow, currCol)) {
            return -1;
        }

        return grid.places[currRow][currCol];
    };
}


/**
 * Game player
 * @constructor
 */
function Play() {
    this.score = 0;
    this.grid = new Blocks(); // set up the Grid
    this.playing = false;

    /**
     * Rest the blocks
     */
    this.configReset = function() {
        this.grid = new Blocks();
        this.score = 0;
        this.grid.addBlock(new Boxes(2));
        this.grid.addBlock(new Boxes(2));
        this.playing = true;
        this.changeBlocks();
    };

    /**
     * Changing the blocks
     */
    this.changeBlocks = function() {
        for (var i = 0; i < this.grid.places.length; i++) {
            for (var j = 0; j < this.grid.places[i].length; j++) {
                var currPiece = this.grid.places[i][j];
                var idString = "#cell-" + i + "-" + j;
                if (currPiece == null) {
                    $(idString + " .text").html("&nbsp;");
                    $(idString).attr("data-cell", 0);
                } else {
                    $(idString + " .text").html("" + currPiece.getValue());
                    $(idString).attr("data-cell", currPiece.value);
                }
            }
        }
    };

    /**
     * Stats the game with Arrow key press in Keyboard
     */
    this.start = function() {
        this.configReset();
        var grid = this.grid;
        var play = this;
        $(document).keydown(function(e) {
            switch(e.which) {
                case 37: // char code of left arrow key
                    grid.move("left");
                    break;
                case 38: // char code of up arrow key
                    grid.move("up");
                    break;
                case 39: // char code of right arrow key
                    grid.move("right");
                    break;
                case 40: // char code of down arrow key
                    grid.move("down");
                    break;
                default: return;
            }
            play.changeBlocks();
            if (grid.canEndGame()) {
                play.playing = false;
                console.log("Game Over !!!");
                $('.grid').prepend('<h2>Game Over!!!</h2>')
                return;
            }
            e.preventDefault();
        });
    };
}

var myplay = new Play();
