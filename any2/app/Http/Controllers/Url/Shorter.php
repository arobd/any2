<?php

namespace Any2\Http\Controllers\Url;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Input;
use Illuminate\Support\Str;

use Any2\Http\Requests;
use Any2\Http\Controllers\Controller;
use Any2\Models\Urls;

class Shorter extends Controller
{
    /**
     * Default function to list the URLs and form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        $data = [
            'urlList'=>Urls::orderBy('updated_at', 'desc')->take(50)->get(),
            'title'=>'URL Shortener'
        ];
        return view('url.home', $data);
    }

    /**
     * Action for shortening the URL
     * @return $this
     */
    public function postShorten() {
        $data = ['url'=>Input::get('url')];
        $rules = ['url'=>'required|url'];
        $validator = Validator::make($data, $rules);

        if( $validator->fails() ) {
            $route = 'url';
            $info = ['warning'=>'Please insert a valid URL.'];
        }
        else {
            try {
                $url = Urls::firstOrNew($data);
                if(empty($url->short_code)) {
                    $url->short_code = Str::random(6);
                    $url->save();
                }
                $route = 'url';
                $info = ['info'=>'URL has been created.'];
            }
            catch(QueryException $e) {
                $route = 'url';
                $info = ['danger'=>'Something has gone wrong, please try again.'];
            }
        }

        return redirect($route)
            ->with($info)
            ->withErrors($validator)
            ->withInput();
    }

    /**
     * Redirects the short URL to actual URL
     * @param $hash
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getRedirect($hash) {
        try {
            $link = Urls::where('short_code', $hash)
                ->first();
            if(empty($link)) {
                $route = 'url';
                $info = ['warning'=>'No shortened URL found'];
            }
            else {
                $route = $link->url;
                $info = [];
                $link->increment('clicks'); // counts the visit
            }
        }
        catch(QueryException $e) {
            $route = 'url';
            $info = ['warning'=>'No shortened URL found'];
        }

        return redirect($route)->with($info);
    }
}
