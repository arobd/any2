<?php

namespace Any2\Http\Controllers\G2048;

use Illuminate\Http\Request;

use Any2\Http\Requests;
use Any2\Http\Controllers\Controller;

class Play extends Controller
{
    /**
     * Load the Game
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        return view('g2048.play', ['title'=>'2048']);
    }
}
