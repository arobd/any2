<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome', ['title'=>'Any2']);
});

Route::group(['prefix' => 'url', 'before' => '', 'namespace' => 'Url'], function() {
    Route::get('/', 'Shorter@getIndex');
    Route::post('/shorten', 'Shorter@postShorten');
    Route::get('/{hash}', 'Shorter@getRedirect');
});

Route::group(['prefix'=>'g2048', 'before' => '', 'namespace' => 'G2048'], function() {
    Route::get('/', 'Play@getIndex');
});