<?php

namespace Any2\Models;

use Illuminate\Database\Eloquent\Model;

class Urls extends Model
{
    protected $table = 'urls';

    protected $fillable = ['url'];
}
