@if(Session::has('alert'))
    <?php $alerts = (Session::get('alert')) ?>
    <div class="alert">
        {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
        <ul>
            @if(is_array($alerts))
                @foreach($alerts as $alert)
                    <li>{{$alert}}</li>
                @endforeach
            @else
                <li>{{ $alerts  }}</li>
            @endif
        </ul>
    </div>
@endif

@if(Session::has('danger'))
    <?php $dangers = (Session::get('danger')) ?>
    <div class="alert alert-danger alert-dismissable">
        {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
        <ul>
            @if(is_array($dangers))
                @foreach($dangers as $danger)
                    <li>{{$dangers}}</li>
                @endforeach
            @else
                <li>{{ $dangers  }}</li>
            @endif
        </ul>
    </div>
@endif

@if(Session::has('success'))
    <?php $successes = (Session::get('success')) ?>
    <div class="alert alert-success alert-dismissable">
        {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
        <ul>
            @if(is_array($successes))
                @foreach($successes as $success)
                    <li>{{$success}}</li>
                @endforeach
            @else
                <li>{{ $successes  }}</li>
            @endif
        </ul>
    </div>
@endif

@if(Session::has('info'))
    <?php $infos = (Session::get('info')) ?>
    <div class="alert alert-info alert-dismissable">
        {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
        <ul>
            @if(is_array($infos))
                @foreach($infos as $info)
                    <li>{{$info}}</li>
                @endforeach
            @else
                <li>{{ $infos  }}</li>
            @endif
        </ul>
    </div>
@endif

@if(Session::has('warning'))
    <?php $warnings = (Session::get('warning')) ?>
    <div class="alert alert-warning alert-dismissable">
        {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
        <ul>
            @if(is_array($warnings))
                @foreach($warnings as $warning)
                    <li>{{$warning}}</li>
                @endforeach
            @else
                <li>{{ $warnings  }}</li>
            @endif
        </ul>
    </div>
@endif

@if($errors->all())
    <div class="alert alert-danger">
        {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
