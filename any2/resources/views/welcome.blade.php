@extends('layout')

@section('content')
    <div class="content">
        <h2 class="content-head is-center">{{$title}}</h2>
        <div class="pure-u-1-1">
            <h3 class="is-center">Please select any one from top menu</h3>
            <p class="is-center">This Any2 Application is developed by <a href="http://arafatbd.net">Arafat Rahman</a></p>
            <p class="is-center">Email: <a href="mailto:opurahman@gmail.com">opurahman@gmail.com</a></p>
            <p class="is-center">H/P: <span class="small">+6016 955 7980</span></p>
        </div>
    </div>
@endsection
