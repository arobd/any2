@extends('layout')
@section('css')
    <link rel="stylesheet" href="{!! URL::to('/') !!}/css/game.css">
@endsection

@section('js')
@endsection

@section('content')
    <div class="content is-center">
        <h2 class="content-head is-center">{{$title}}</h2>
        <div class="pure-u-1-1 is-center"><p>Press any arrow key to start  or <a href="{!! URL::to('g2048') !!}">Reload</a></p></div>
        <div class="pure-g" style="width: 500px;">
            <div class="pure-u-1-1">
                <div class="grid"></div>
            </div>
        </div>
    </div>
@endsection

@section('footerJs')
    <script src="{!! URL::to('/') !!}/js/game.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var j = 0;
            var k = 0;
            for(i = 0; i < 16; i++) {
                if(i > 3 && i % 4 == 0) {
                    j = j + 1;
                    k = 0;
                }

                var boxes = $('<div class="boxs" id="cell-'+j+'-'+k+'"><div class="text"></div></div>');
                k = k + 1;

                $('.grid').append(boxes);
            }
        });

        myplay.start();
    </script>
@endsection