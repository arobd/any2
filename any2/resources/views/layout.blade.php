<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
    <![endif]-->
    <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">
    <!--<![endif]-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="{!! URL::to('/') !!}/css/layouts/marketing-old-ie.css">
    <![endif]-->
    <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="{!! URL::to('/') !!}/css/layouts/marketing.css">
    <!--<![endif]-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
    @yield('js')
    @yield('css')
</head>
<body>

<div class="header">
    <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
        <a class="pure-menu-heading" href="">{{$title}}</a>

        <ul class="pure-menu-list">
            <li class="pure-menu-item"><a href="{!! URL::to('/url') !!}" class="pure-menu-link">URL Shortener</a></li>
            <li class="pure-menu-item"><a href="{!! URL::to('/g2048') !!}" class="pure-menu-link">2048 Game</a></li>
        </ul>
    </div>
</div>

<div class="content-wrapper">
    @include('alert')
    @yield('content')
</div>

<div class="footer l-box is-center">
    &copy; 2015 All rights reserved. <a href="http://arafatbd.net" class="">Arafat Rahman</a>
</div>

@yield('footerCss')
@yield('footerJs')

</body>
</html>