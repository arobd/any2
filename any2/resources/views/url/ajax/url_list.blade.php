<div class="pure-control-group">
    <label for="url{{$url->id}}"><i class="fa fa-anchor"></i> Short url:</label>
    <input type="text" id="url{{$url->id}}" value="{{URL::to('url')}}/{{$url->short_code}}" readonly> <span class="small">Visit count: {{$url->clicks}}</span>
    <span class="pull-right">Long URL: {{$url->url}}</span>
    <hr>
</div>