{!! Form::open(['class'=>'pure-form pure-form-aligned']) !!}
    <div id="list_url">
        @foreach($urlList as $url)
            @include('url.ajax.url_list')
        @endforeach
    </div>
{!! Form::close() !!}