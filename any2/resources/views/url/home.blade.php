@extends('layout')
@section('content')
    <div class="content">
        <h2 class="content-head is-center">{{$title}}</h2>
        {!! Form::open(['url'=>'url/shorten', 'method'=>'post', 'name'=>'short', 'id'=>'short', 'class'=>'pure-form']) !!}
        <div class="pure-g">
            <div class="pure-u-1-12">
                <label for="url">Long URL</label>
            </div>
            <div class="pure-u-7-12">
                <input type="text" name="url" id="url" class="pure-input-rounded pure-input-1" required>
            </div>
            <div class="pure-u-1-12"></div>
            <div class="pure-u-1-12 is-center"><button type="submit" class="pure-button"><i class="fa fa-anchor"></i> Shorten</button></div>
        </div>
        {!! Form::close() !!}
        @include('url.url_list')
    </div>
@endsection

@section('footerJs')
    {{--$('#short').submit(function(e) {
    var postData = $(this).serializeArray();
    var formURL = $(this).prop('action');
    $.ajax({
    url : formURL,
    type : "POST",
    data : postData,
    success: function(data, textStatus, jqXHR) {
    $('#list_url').prepend(data);
    },
    error : function(jqXHR, textStatus, errorThrown) {
    console.log(errorThrown);
    }
    });
    e.preventDefault();
    });--}}
    <script type="text/javascript">
        $(document).ready( function() {
            $('#list_url').on('click', 'input', function() { this.select(); });
        });
    </script>
@endsection