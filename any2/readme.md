## Any2

This is a repository for interview test.

## URL Shorter

To Shorten an URL.

[Demo](http://arafatbd.net/any2/url)

## 2048 Game

A famous game.

[Demo](http://arafatbd.net/any2/g2048)

### License

The Any2 is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
